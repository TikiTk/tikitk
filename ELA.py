import argparse
import json
import logging
import os

import cv2
import numpy as np
from PIL import Image, ImageChops
from skimage.measure import _structural_similarity as ssim

logger = logging.getLogger(__name__)
TMP_EXT = ".ELA_TMP.jpg"
SAVE_REL_DIR = "generated"
SAVE_ELA_REL_DIR = "ELA_and_json"
EXT = ".jpg"
quality = 85
ErrorScale = 10
data = {}
KEY = 'difference_values'
data[KEY] = []

parser = argparse.ArgumentParser(description="""
Performs Error Level Analysis over a directory of images
""")
parser.add_argument('--dir', dest='directory', required=True,
                    help='path to the directory containing the images')
parser.add_argument('--quality', dest='quality',
                    help='quality used by the jpep compression alg.',
                    default=90)


def calculateDifferenceUsingSSIMandMSE(original_image, compressed_image, filename):
    compressed_image = cv2.imread(compressed_image)
    compressed_image = cv2.cvtColor(compressed_image, cv2.COLOR_BGR2GRAY)
    original_image = cv2.imread(original_image)
    original_image = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)

    def calculateMSE(originalImage, compressedImage):
        error = np.sum((originalImage.astype("float") - compressedImage.astype("float")) ** 2)
        error /= float(originalImage.shape[0] * originalImage.shape[1])
        return error

    MSE = calculateMSE(original_image, compressed_image)
    SSIM = ssim.compare_ssim(original_image, compressed_image)

    data[KEY].append({
        'filename': filename,
        'MSE': MSE,
        'SSIM': SSIM
    })

    return {"SSIM": SSIM, "MSE": MSE}


def computeELA(filename, originalDirectory, destinationDirectory):
    try:
        basename, ext = os.path.splitdrive(filename)
        original_filename = os.path.join(originalDirectory, filename)
        temporary_filename = os.path.join(destinationDirectory, filename[0:filename.index('.')] + basename + TMP_EXT)
        difference_image = os.path.join(destinationDirectory[0:destinationDirectory.index(SAVE_REL_DIR)], SAVE_ELA_REL_DIR)
        difference_image = os.path.join(difference_image,
                                        filename[0:filename.index('.')] + "." + basename + 'ela_difference' + EXT)

        original_image = Image.open(original_filename)
        original_image.save((temporary_filename), format='JPEG', quality=quality)
        temporary = Image.open(temporary_filename)
        difference = ImageChops.difference(original_image, temporary)
        d = difference.load()

        WIDTH, HEIGHT = difference.size
        for x in range(WIDTH):
            for y in range(HEIGHT):
                d[x, y] = tuple(k * ErrorScale for k in d[x, y])

        difference.save(difference_image, format='JPEG')
        result = calculateDifferenceUsingSSIMandMSE(original_filename, temporary_filename, filename)
        print("The values of image " + filename, result)
    except Exception as e:
        logger.error("Cannot parse image: {}! Error: {}".format(filename, e))


# computeELA('D.jpg','F:\Documents\SNE-RP2\ELA_Python','F:\Documents\SNE-RP2\ELA_Python\generated')
# with open('data.txt', 'w') as output:
#     json.dump(data, output)

def run():
    args = parser.parse_args()
    dirc = args.directory
    quality = args.quality

    temporary_directory = os.path.join(dirc, SAVE_REL_DIR)
    ela_directory = os.path.join(dirc, SAVE_ELA_REL_DIR)

    print("Performing ELA on images at %s" % dirc)

    if not os.path.exists(temporary_directory):
        os.makedirs(temporary_directory)
    if not os.path.exists(ela_directory):
        os.makedirs(ela_directory)

    for f in [file for file in os.listdir(dirc) if file.endswith(".jpg") or file.endswith(".jpeg")]:
        computeELA(f, dirc, temporary_directory)

    print("Finished!")
    print("Head to %s/%s to check the results!" % (dirc, SAVE_ELA_REL_DIR))
    with open(os.path.join(ela_directory, 'data.txt'), 'w') as output:
        json.dump(data, output)


if __name__ == '__main__':
    run()
