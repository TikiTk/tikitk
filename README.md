# ELA
## What is it

## Requirements
* Python 3 (maybe python 2, not tested)
* Virtual environment (Optional, but strongly recommended)

## Before run

1. Create new virtual environment
```bash
virtualenv -p python3 venv
```

2. Activate virtual environment
```bash
source venv/bin/activate
```

3. Install requirements
```bash
pip install -r requirements.txt
```

3. Run script
```bash
python ELA.py --dir /path/to/dir/with/images

```

4. ???

5. Profit!

6. After finishing in folder `/path/to/dir/with/images` you will find new folder `ELA_and_json`.